import Connections from '../globals/Connections'
import IWsMetaData from './IWsMetaDataForClient'
export default class MessageController {
  /**
   * Получаем сообщение от одного клиента и передаем другому
   */
  public static async handleMessageToClient(message: IWsMetaData) {
    const targetClient: IWsMetaData | undefined = Connections.connections.find(
      (conn) => {
        if (conn.meta.wsID === message.meta.to) {
          return conn
        }
      }
    )

    if (!targetClient) throw new Error('Нет такого клиента')

    const wsMetaDataForClient = {
      meta: targetClient.meta,
      data: message.data,
    }

    targetClient.ws.send(JSON.stringify(wsMetaDataForClient))
  }

  /**
   * Обрабатываем сообщение от клиента на сервер
   */
  public static async getMessageForAll(message: any) {
    console.log('сообщение для всех')
  }
}
