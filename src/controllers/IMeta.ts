export default interface IMeta {
  wsID: string
  userName: string
  token: string
  to?: string
}
