import IMeta from './IMeta'

export default interface IWsMetaData {
  meta: IMeta
  data?: any
  ws?: any
}
