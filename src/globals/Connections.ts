import IWsMetaData from '../controllers/IWsMetaDataForClient'

interface Connections {
  connections: Array<IWsMetaData>
  connectionIDNumber: number
}
const Connections: Connections = {
  connections: [],
  connectionIDNumber: 0,
}
export default Connections
