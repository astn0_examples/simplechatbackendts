import WebSocket from 'ws'
import IMeta from './controllers/IMeta'
import IWsMetaData from './controllers/IWsMetaDataForClient'
import Connections from './globals/Connections'
import WsRoutes from './Routes/WsRoutes'
import dotenv from 'dotenv'
import Config from './globals/Config'

dotenv.config()
Config.wsPort = process.env.wsPort as string

const wss: WebSocket.Server = new WebSocket.Server({
  port: parseInt(Config.wsPort),
})

wss.on('connection', (ws: WebSocket) => {
  Connections.connectionIDNumber++

  const meta: IMeta = {
    wsID: Connections.connectionIDNumber.toString(),
    userName: '',
    token: '',
  }
  const wsMetaData: IWsMetaData = {
    meta,
    ws,
  }
  const wsMetaDataForClient: IWsMetaData = {
    meta,
    data: 'Соединение установлено',
  }

  Connections.connections.push(wsMetaData)

  ws.on('message', (message: any) => {
    const messageFromClient = JSON.parse(message)
    WsRoutes.doRoute(messageFromClient)
  })

  ws.send(JSON.stringify(wsMetaDataForClient)) // todo: убери в контроллер
})
