import MessageController from '../controllers/MessageController'
export default class WsRoutes {
  public static async doRoute(message: any) {
    if (!message.meta.to) {
      MessageController.getMessageForAll(message)
    } else {
      MessageController.handleMessageToClient(message)
    }
  }
}
